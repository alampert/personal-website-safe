#!/bin/bash
HOST='ftp.lampert.ca'
USER='lampertSolutionsJenkins'

ncftpput -R -v -u $USER -p $PASSWD $HOST / dist

ncftpput -R -v -u $USER -p $PASSWD $HOST /dist/ src/server

# ftp -vn $HOST <<END_SCRIPT
# quote USER $USER
# quote PASS $PASSWD
# prompt
# mput dist/*
# mput dist/assets/*
# quit
# END_SCRIPT
# exit 0

# cd src

# ftp -vn $HOST <<END_SCRIPT
# quote USER $USER
# quote PASS $PASSWD
# prompt
# mput server/*
# quit
# END_SCRIPT
# exit 0
