/**
 * Created by andrew on 04/04/17.
 */
import { AlicesE2EPage } from './alicesPage.po';

describe ( 'Alices page', () => {
	let page : AlicesE2EPage;

	beforeAll ( () => {
		page = new AlicesE2EPage ();
		page.navigateTo ();
	} );

	describe ( 'Content', () => {

		it ( 'should not contain profanities', function () {
			expect ( page.getAllText() ).not.toContain("fuck");
			expect ( page.getAllText() ).not.toContain("shit");
			expect ( page.getAllText() ).not.toContain("damn");
		} );

	} );

} );
