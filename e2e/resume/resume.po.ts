/**
 * Created by andrew on 04/04/17.
 */
import { browser, element, by } from 'protractor';

export class ResumeE2EPage {
	navigateTo() {
		return browser.get('/#/resume');
	}

	getJumbotronHeader(){
		return element(by.css('app-resume div.jumbotron')).getText();
	}

	getQuickSkillsText() {
		return element(by.css('app-resume #quick_skills')).getText();
	}

	getModeTitle(){
		return element(by.css('app-resume #mode_title')).getText();
	}

	switchMode(){
		element(by.css('app-resume #mode_switch')).click();
	}

	getAllText(){
		return element(by.css('app-resume')).getText();
	}
}
