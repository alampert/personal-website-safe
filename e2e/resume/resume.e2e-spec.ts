/**
 * Created by andrew on 04/04/17.
 */
import { ResumeE2EPage } from './resume.po';

describe ( 'resume App', () => {
	let page : ResumeE2EPage;

	beforeAll ( () => {
		spyOn ( console, 'error' );

		page = new ResumeE2EPage ();
		page.navigateTo ();

	} );

	describe ( 'jumbotron', () => {
		it ( 'should contain "Hello World" at the top', () => {

			expect ( page.getJumbotronHeader () ).toContain ( "Hello, world!" );

		} );
	} );

	describe ( 'quick skills list', () => {

		it ( 'should contain Programming Languages', () => {

			expect ( page.getQuickSkillsText () ).toContain ( "Programming Languages" );

		} );

		it ( 'should contain Frameworks', () => {

			expect ( page.getQuickSkillsText () ).toContain ( "Frameworks" );

		} );

		it ( 'should contain Software', () => {

			expect ( page.getQuickSkillsText () ).toContain ( "Software" );

		} );

		// Items

		it ( 'should contain C++', () => {

			expect ( page.getQuickSkillsText () ).toContain ( "C++" );

		} );

		it ( 'should contain Slack', () => {

			expect ( page.getQuickSkillsText () ).toContain ( "Slack" );

		} );

		it ( 'should contain Microsoft Word', () => {

			expect ( page.getQuickSkillsText () ).toContain ( "Microsoft Word" );

		} );

		it ( 'should contain Microsoft Power Point', () => {

			expect ( page.getQuickSkillsText () ).toContain ( "Microsoft Power Point" );

		} );

		it ( 'should contain Sublime', () => {

			expect ( page.getQuickSkillsText () ).toContain ( "Sublime" );

		} );

		it ( 'should contain Bootstrap', () => {

			expect ( page.getQuickSkillsText () ).toContain ( "Bootstrap" );

		} );

		it ( 'should contain Angular 2', () => {

			expect ( page.getQuickSkillsText () ).toContain ( "Angular 2" );

		} );
	} );

	describe ( 'Content', () => {
		it ( 'should contain 2nd Lieutenant', () => {
			expect ( page.getAllText () ).toContain ( "2nd Lieutenant" );
		} );
		it ( 'should contain Web Developer', () => {
			expect ( page.getAllText () ).toContain ( "Web Developer" );
		} );
		it ( 'should contain Webmaster', () => {
			expect ( page.getAllText () ).toContain ( "Webmaster" );
		} );
		it ( 'should not contain Web master', () => {
			expect ( page.getAllText () ).not.toContain ( "Web Master" );
			expect ( page.getAllText () ).not.toContain ( "Web master" );
			expect ( page.getAllText () ).not.toContain ( "web master" );
		} );
		it ( 'should contain Westmount Collegiate Institute', () => {
			expect ( page.getAllText () ).toContain ( "Westmount Collegiate Institute" );
		} );
		it ( 'should contain University of Western Ontario', () => {
			expect ( page.getAllText () ).toContain ( "University of Western Ontario" );
		} );
		it ( 'should not contain <blockquote>', () => {
			expect ( page.getAllText () ).not.toContain ( "<blockquote>" );
			expect ( page.getAllText () ).not.toContain ( "</blockquote>" );
		} );
	} );

	describe ( 'Sections', () => {
		// it( 'should contain an Awards section', () => {
		// 	expect( page.getAllText()).toContain("Awards");
		// });
	} );

	describe ( 'general errors', () => {

		it ( 'should not be called', () => {
			expect ( console.error ).not.toHaveBeenCalled ();
		} );

		it ( 'should fail', function () {
			console.error ( "Oops!" );
			expect ( console.error ).toHaveBeenCalled ();
		} );

		it ( 'should not contain profanities', function () {
			expect ( page.getAllText() ).not.toContain("fuck");
			expect ( page.getAllText() ).not.toContain("shit");
			expect ( page.getAllText() ).not.toContain("damn");
		} );

	} );
} );
