/**
 * Created by andrew on 04/04/17.
 */
import { browser, element, by } from 'protractor';

export class NavigationE2EPage {
	navigateToHome() {
		return browser.get('/#/home');
	}

	getAllText(){
		return element(by.css('app-root')).getText();
	}

	getNavText(){
		return element(by.css('app-navbar')).getText();
	}
}
