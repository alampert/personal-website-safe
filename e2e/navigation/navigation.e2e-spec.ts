/**
 * Created by andrew on 04/04/17.
 */
import { NavigationE2EPage } from './navigation.po';

describe ( 'Navigation Component', () => {
	let nav : NavigationE2EPage;

	beforeAll ( () => {
		nav = new NavigationE2EPage ();
	} );


	it ( 'should initiate', () => {
		expect ( nav ).toBeTruthy();
	} );
} );
