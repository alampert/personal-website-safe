export class GroundSchoolUserModel {
	first_name: string;
	last_name: string;
	member_id: number;
	rank: string;
	testAttempts?: any[];
	averageTestScore?: number;
}
