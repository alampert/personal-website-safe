import { Component, OnInit } from '@angular/core';
import { SITENAME, VERSION } from '../../globals';
@Component({
	selector: 'app-home',
	templateUrl: 'home.page.html',
})

export class HomePage implements OnInit {

	SITENAME = SITENAME;
	VERSION = VERSION;

	constructor() {
	}

	ngOnInit() {

	}


}
