import { Component, OnInit, Input } from '@angular/core';
import { JobModel } from '../../../../models/job.model';
import { SkillsModel } from '../../../../models/skills.model';
import { RESUMEMODE, JOBTYPE } from '../../../../globals';
import { ResumeService } from '../../../../services/resume/resume.service';

@Component({
	selector: 'app-resume-job-highlight',
	templateUrl: 'job-highlight.component.html'
})

export class JobHighlightComponent implements OnInit {

	@Input() public job: JobModel;
	@Input() public mode: RESUMEMODE = RESUMEMODE.CV;
	public dateFormatString = 'MMM yyyy';

	RESUMEMODE = RESUMEMODE;
	JOBTYPE = JOBTYPE;

	constructor(
		private resumeService: ResumeService
	) { }

	ngOnInit() {
		if (this.job && !this.job.skills && this.job.id) {
			this.resumeService.getPositionSkills(this.job.id).subscribe(skills => {
				this.job.skills = skills;
			});
		}
	}
}
