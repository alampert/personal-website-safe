import { Component, OnInit, Input } from '@angular/core';

@Component({
	selector: 'app-resume-contact-information',
	templateUrl: 'contact-information.component.html'
})

export class ContactInformationComponent implements OnInit {

	public contactInformation: { method: string, value: string }[];

	constructor() {
		this.contactInformation = [];
	}

	ngOnInit() {
		this.contactInformation = this.getContactInformation();
	}

	getContactInformation(): { method: string, value: string }[] {
		return [
			{
				method: 'Phone Number',
				value: `647-231-4644`
			},
			{
				method: 'E-Mail',
				value: `Andrew@Lampert.ca`
			}
		];
	}
}
