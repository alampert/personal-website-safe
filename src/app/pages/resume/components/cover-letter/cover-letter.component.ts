import { Component, OnInit, Input } from '@angular/core';
import { COVERLETTERMODE } from '../../../../globals';
import { environment } from '../../../../../environments/environment';

@Component({
	selector: 'app-resume-cover-letter',
	templateUrl: 'cover-letter.component.html'
})

export class CoverLetterComponent implements OnInit {

	COVERLETTERMODE = COVERLETTERMODE;

	coverLetterMode: COVERLETTERMODE = COVERLETTERMODE.DEFAULT;

	public jobPositionName: string;

	constructor() {
		this.coverLetterMode = COVERLETTERMODE.DIGITALECHIDNA;	// Digital Ekidna

		// TODO: FILL IN
		this.jobPositionName = `Intermediate Backend Developer`;

		if (!environment.isLocal) {
			this.coverLetterMode = COVERLETTERMODE.DEFAULT;
		}
	}

	ngOnInit() { }
}
