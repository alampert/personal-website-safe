import { NgModule } from '@angular/core';

import { CommonModule } from '@angular/common';

import { ResumePage } from './resume.page';
import { ResumeRoutingModule } from './resume.routing';
import { CoverLetterComponent } from './components/cover-letter/cover-letter.component';
import { CoverLetterContainerComponent } from './components/cover-letter/cover-letter-container.component';
import { QuickSkillsComponent } from './components/quick-skills/quick-skills.component';
import { JobHighlightComponent } from './components/job-highlight/job-highlight.component';
import { SharedModule } from '../../shared/shared.module';
import { ContactInformationComponent } from './components/contact-information/contact-information.component';

@NgModule({
	imports: [
		CommonModule,
		ResumeRoutingModule,
		SharedModule
	],
	exports: [],
	declarations: [
		ResumePage,
		CoverLetterComponent,
		QuickSkillsComponent,
		JobHighlightComponent,
		CoverLetterContainerComponent,
		ContactInformationComponent
	],
	providers: [],
})
export class ResumeModule { }
