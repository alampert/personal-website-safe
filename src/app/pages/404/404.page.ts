import { Component, OnInit } from '@angular/core';

@Component({
	selector: 'app-404-page',
	templateUrl: '404.page.html'
})

export class Missing404Page implements OnInit {
	constructor() { }

	ngOnInit() { }
}
