import { Component, OnInit } from '@angular/core';
import { WishlistService } from '../../services/wishlist/wishlist.service';

@Component({
	selector: 'app-wishlist',
	templateUrl: 'wishlist.page.html'
})

export class WishlistPage implements OnInit {

	public wishlist: any[];

	constructor(
		private wishlistService: WishlistService
	) { }

	ngOnInit() {
		this.wishlistService.getWishlist().subscribe(list => {
			this.wishlist = list;
		});
	}

}
