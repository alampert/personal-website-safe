import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import { LectureModel } from '../../../models/lecture.model';
import { MONTH } from '../../../globals';
import { environment } from '../../../../environments/environment';
@Component({
	selector: 'app-ground-school',
	templateUrl: 'ground-school.component.html'
})

export class GroundSchoolComponent implements OnInit {

	// VARIABLES

	MONTH = MONTH;
	public lectures: LectureModel[];
	public courseSubjects = [
		'Aircraft Structures',
		'Theory of Flight',
		'Air Law',
		'General Airmanship',
		'Meterology',
		'Air Navigation',
		'Aero Engines'
	];

	public test1Results = {
		datasets: [{
			data: [
				11,
				16,
				7,
				3,
				14
			],
			backgroundColor: [
				'#FF6384',
				'#4BC0C0',
				'#FFCE56',
				'#E7E9ED',
				'#36A2EB'
			],
			label: 'My dataset'
		}],
		labels: [
			'Red',
			'Green',
			'Yellow',
			'Grey',
			'Blue'
		]
	};

	// CONSTRUCTORS

	constructor() {
		this.lectures = [];
	}

	ngOnInit() {
		this.getLectures().subscribe((lectures: LectureModel[]) => {
			this.lectures = lectures.sort((a, b) => {
				return b.date.valueOf() - a.date.valueOf();
			});
		});
	}

	// METHODS

	getLectures(): Observable<LectureModel[]> {
		return new Observable(subscriber => {
			subscriber.next([
				{
					id: 0,
					date: new Date(2017, 9, 10),
					content: [
						{
							text: 'TOF 402.01',
							href: `${environment.serverBaseURL}/assets/files/ground-school-1718/TOF402-01.pdf`
						},
						{
							text: 'TOF 402.02',
							href: `${environment.serverBaseURL}/assets/files/ground-school-1718/TOF402-02.pdf`
						},
						{
							text: 'TOF 402.03',
							href: `${environment.serverBaseURL}/assets/files/ground-school-1718/TOF402-03.pdf`
						},
						{
							text: 'TOF 402.04',
							href: `${environment.serverBaseURL}/assets/files/ground-school-1718/TOF402-04.pdf`
						}
					]
				},

				{
					id: 0,
					date: new Date(2017, 9, 17),
					content: [
						{ text: 'Test 1 (TOF 402.01 - 402.02)' },
						{
							text: 'TOF 402.05',
							href: `${environment.serverBaseURL}/assets/files/ground-school-1718/TOF402-05.pdf`
						},
						{
							text: 'TOF 402.06',
							href: `${environment.serverBaseURL}/assets/files/ground-school-1718/TOF402-06.pdf`
						},
						{
							text: 'TOF 402.07',
							href: `${environment.serverBaseURL}/assets/files/ground-school-1718/TOF402-07.pdf`
						}
					]
				},

				{
					id: 0,
					date: new Date(2017, 9, 24),
					content: [
						{
							text: 'TOF 402.08',
							href: `${environment.serverBaseURL}/assets/files/ground-school-1718/TOF402-08.pdf`
						},
						{
							text: 'TOF 402.09',
							href: `${environment.serverBaseURL}/assets/files/ground-school-1718/TOF402-09.pdf`
						},
						{
							text: 'TOF 402.10',
							href: `${environment.serverBaseURL}/assets/files/ground-school-1718/TOF402-10.pdf`
						}
					]
				},

				{
					id: 0,
					date: new Date(2017, 9, 31),
					content: [
						{ text: 'Cancelled' }
					]
				},

				{
					id: 0,
					date: new Date(2017, 10, 7),
					content: [
						{
							text: 'AER 401.01',
							href: `${environment.serverBaseURL}/assets/files/ground-school-1718/AER401-01.pdf`
						},
						{
							text: 'AER 401.02',
							href: `${environment.serverBaseURL}/assets/files/ground-school-1718/AER401-02.pdf`
						},
						{
							text: 'AER 401.03',
							href: `${environment.serverBaseURL}/assets/files/ground-school-1718/AER401-03.pdf`
						},
					]
				},

				{
					id: 0,
					date: new Date(2017, 10, 14),
					content: [
						{ text: 'Test 2 (TOF 402.03 - 402.10' },
						{
							text: 'AER 401.04',
							href: `${environment.serverBaseURL}/assets/files/ground-school-1718/AER401-04.pdf`
						},
						{
							text: 'MET 403.01',
							href: `${environment.serverBaseURL}/assets/files/ground-school-1718/MET403-01.pdf`
						},
						{
							text: 'MET 403.02',
							href: `${environment.serverBaseURL}/assets/files/ground-school-1718/MET403-02.pdf`
						},
					]
				},

				{
					id: 0,
					date: new Date(2017, 10, 21),
					content: [
						{ text: 'Cancelled' },
					]
				},

				{
					id: 0,
					date: new Date(2017, 10, 28),
					content: [
						{ text: 'Test 3 (AER 401.01 - 401.04)' },
						{
							text: 'MET 403.03',
							href: `${environment.serverBaseURL}/assets/files/ground-school-1718/MET403-03.pdf`
						},
						{
							text: 'MET 403.04',
							href: `${environment.serverBaseURL}/assets/files/ground-school-1718/MET403-04.pdf`
						},
						{
							text: 'MET 403.05',
							href: `${environment.serverBaseURL}/assets/files/ground-school-1718/MET403-05.pdf`
						},
					]
				},

				{
					id: 0,
					date: new Date(2017, 11, 5),
					content: [
						{
							text: 'MET 403.06',
							href: `${environment.serverBaseURL}/assets/files/ground-school-1718/MET403-06.pdf`
						},
						{
							text: 'MET 403.07',
							href: `${environment.serverBaseURL}/assets/files/ground-school-1718/MET403-07.pdf`
						},
						{
							text: 'MET 403.08',
							href: `${environment.serverBaseURL}/assets/files/ground-school-1718/MET403-08.pdf`
						},
						{
							text: 'MET 403.09',
							href: `${environment.serverBaseURL}/assets/files/ground-school-1718/MET403-09.pdf`
						},
						{
							text: 'MET 403.10',
							href: `${environment.serverBaseURL}/assets/files/ground-school-1718/MET403-10.pdf`
						},
						{
							text: 'MET 403.11',
							href: `${environment.serverBaseURL}/assets/files/ground-school-1718/MET403-11.pdf`
						},
					]
				},

				{
					id: 0,
					date: new Date(2017, 11, 12),
					content: [
						{
							text: 'CANCELLED DUE TO WEATHER',
						},
						{
							text: 'Clouds Lecture',
							href: `${environment.serverBaseURL}/assets/files/ground-school-1718/GS2017-METClouds.pdf`
						},
						{
							text: 'MET 403.12',
							href: `${environment.serverBaseURL}/assets/files/ground-school-1718/MET403-12.pdf`
						},
						{
							text: 'MET 403.13',
							href: `${environment.serverBaseURL}/assets/files/ground-school-1718/MET403-13.pdf`
						},
						{
							text: 'Navigation Lectures Coming Soon!',
						},
					]
				},

				{
					id: 0,
					date: new Date(2017, 11, 19),
					content: [
						{
							text: 'Meterology Test',
						},
						{
							text: 'NAV 404.01',
							href: `${environment.serverBaseURL}/assets/files/ground-school-1718/NAV404-01.ppsx`
						},
						{
							text: 'NAV 404.02',
							href: `${environment.serverBaseURL}/assets/files/ground-school-1718/NAV404-02.ppsx`
						},
						{
							text: 'NAV 404.03',
							href: `${environment.serverBaseURL}/assets/files/ground-school-1718/NAV404-03.ppsx`
						},
					]
				},
				{
					id: 0,
					date: new Date(2017, 11, 26),
					content: [
						{
							text: 'NO CLASS! Just homework.',
						},
						{
							text: 'NAV 404.04',
							href: `${environment.serverBaseURL}/assets/files/ground-school-1718/NAV404-04.ppsx`
						},
						{
							text: 'ENG 407.01 - PPS Only',
							href: `${environment.serverBaseURL}/assets/files/ground-school-1718/ENG407-01.ppsx`
						},
						{
							text: 'ENG 407.02 - PPS Only',
							href: `${environment.serverBaseURL}/assets/files/ground-school-1718/ENG407-02.ppsx`
						},
						{
							text: 'ENG 407.03 - PPS Only',
							href: `${environment.serverBaseURL}/assets/files/ground-school-1718/ENG407-03.ppsx`
						},
						{
							text: 'ENG 407.04 - PPS Only',
							href: `${environment.serverBaseURL}/assets/files/ground-school-1718/ENG407-04.ppsx`
						},
						{
							text: 'ENG 407.05 - PPS Only',
							href: `${environment.serverBaseURL}/assets/files/ground-school-1718/ENG407-05.ppsx`
						},
					]
				},
				{
					id: 0,
					date: new Date(2018, 0, 2),
					content: [
						{
							text: 'NO CLASS! Just homework.',
						},
						{
							text: 'Navigation Assignment (Due 09 Jan)',
							href: `${environment.serverBaseURL}/assets/files/ground-school-1718/NavAssignment.pdf`
						},
						{
							text: 'Test 1 Answers',
							href: `${environment.serverBaseURL}/assets/files/ground-school-1718/Test1-ans.pdf`
						},
						{
							text: 'Test 2 Answers',
							href: `${environment.serverBaseURL}/assets/files/ground-school-1718/Test2-ans.pdf`
						},
						{
							text: 'Test 3 Answers',
							href: `${environment.serverBaseURL}/assets/files/ground-school-1718/Test3-ans.pdf`
						},
						{
							text: 'Test 4 Answers',
							href: `${environment.serverBaseURL}/assets/files/ground-school-1718/Test4-ans.pdf`
						},
					]
				},
				{
					id: 0,
					date: new Date(2018, 0, 11),
					content: [
						{
							text: 'Past Entrance Exams',
							href: `${environment.serverBaseURL}/assets/files/ground-school-1718/GSPastEntranceExams.zip`
						},
					]
				}
			]);
			subscriber.complete();
		});
	}
}
