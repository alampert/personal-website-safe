import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TrainingComponent } from './training.component';
import { GroundSchoolComponent } from './ground-school/ground-school.component';
import { CourseComponent } from './course/course.component';
import { StatisticsComponent } from './statistics/statistics.component';

const routes: Routes = [
	{ path: '', component: TrainingComponent },
	{
		path: 'course',
		children: [
			{ path: ':courseId', component: CourseComponent },
		]
	},
	{ path: 'groundschool', component: GroundSchoolComponent },
	{ path: ':groundschool/statistics', component: StatisticsComponent }
];

@NgModule({
	imports: [RouterModule.forChild(routes)],
	exports: [RouterModule],
})
export class TrainingRoutingModule { }

export const routedComponents = [TrainingComponent];
