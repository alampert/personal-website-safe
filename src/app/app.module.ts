/* Core Components */
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpModule } from '@angular/http';

/* App Components */
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app.routing';

/* Shared */
import { SharedModule } from './shared/shared.module';

/* Pages & Layouts */
import { HomePage } from './pages/home/home.page';
import { Missing404Page } from './pages/404/404.page';
import { MainLayoutComponent } from './layouts/main-layout.component';

/* Prime NG */
import { PanelModule, MenuModule, TabMenuModule, MenuItem } from 'primeng/primeng';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { MatToolbarModule, MatButtonModule, MatCardModule, MatMenuModule } from '@angular/material';
import { WishlistPage } from './pages/wishlist/wishlist.page';
@NgModule({
	imports: [
		CommonModule,
		BrowserModule,
		BrowserAnimationsModule,
		RouterModule,
		HttpModule,

		AppRoutingModule,

		PanelModule,
		MenuModule,
		TabMenuModule,

		MatToolbarModule,
		MatButtonModule,
		MatCardModule,
		MatMenuModule,

		SharedModule.forRoot(),
	],
	declarations: [
		AppComponent,

		MainLayoutComponent,

		HomePage,
		WishlistPage,

		Missing404Page
	],
	providers: [

	],
	bootstrap: [
		AppComponent
	],
	schemas: [
		CUSTOM_ELEMENTS_SCHEMA
	]
})
export class AppModule { }
