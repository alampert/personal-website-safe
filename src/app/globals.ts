import { Injectable } from '@angular/core';

@Injectable()
export class Globals {
}

export const SITENAME = 'Lampert.Solutions';
export const VERSION = '4.2.3';

export enum MONTH {
	'Jan' = 0,
	'Feb' = 1,
	'March' = 2,
	'April' = 3,
	'May' = 4,
	'June' = 5,
	'July' = 6,
	'Aug' = 7,
	'Sept' = 8,
	'Oct' = 9,
	'Nov' = 10,
	'Dec' = 11
}

export enum RESUMEMODE {
	CV = 'Curriculum Vitae',
	RESUME = 'Resume'
}

export enum JOBTYPE {
	WORK = 'Work',
	VOLUNTEER = 'Volunteer',
	EDUCATION = 'Education',
	PROJECT = 'Project'
}

export enum HTTPQUERYTYPE {
	GET = 'GET',
	POST = 'POST'
}

export enum COVERLETTERMODE{
	DEFAULT = '',
	DIGITALECHIDNA = 'Digital Echidna'
}
