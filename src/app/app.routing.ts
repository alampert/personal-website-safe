import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AppComponent } from './app.component';
import { HomePage } from './pages/home/home.page';
import { MainLayoutComponent } from './layouts/main-layout.component';
import { Missing404Page } from './pages/404/404.page';
import { WishlistPage } from './pages/wishlist/wishlist.page';


const routes: Routes = [
	{
		path: '',
		component: MainLayoutComponent,
		children: [
			{
				path: '',
				component: HomePage
			},
			{
				path: 'training',
				loadChildren: './pages/training/training.module#TrainingModule'
			},
			{
				path: 'resume',
				loadChildren: './pages/resume/resume.module#ResumeModule'
			},
			{
				path: 'wishlist',
				component: WishlistPage
			},
			{
				path: '**',
				component: Missing404Page
			}
		]
	},
];

@NgModule({
	imports: [
		RouterModule.forRoot(routes, {useHash: true})
	],
	exports: [
		RouterModule
	],
})
export class AppRoutingModule { }
