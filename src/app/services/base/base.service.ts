import { Injectable } from '@angular/core';
import { ToastyService } from 'ng2-toasty';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class BaseService {

	constructor(
		protected toastyService: ToastyService
	) { }

	handleError(error: Response | any) {
		if (!!(this.toastyService)) {
			this.toastyService.error('An error occured!');
		}
		debugger;
		if (!!error) {
			console.error(error);
			return Observable.throw(error);
		} else {
			return Observable.throw('Error Occured');
		}
	}
}
