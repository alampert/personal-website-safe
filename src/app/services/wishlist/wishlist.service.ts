import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { BaseService } from '../base/base.service';
import { ToastyService } from 'ng2-toasty';
import { ServerService } from '../server/server.service';
import { HTTPQUERYTYPE } from '../../globals';

@Injectable()
export class WishlistService extends ServerService {

	constructor(
		protected http: Http,
		protected toastyService: ToastyService
	) {
		super(http, toastyService);
	}

	getWishlist(): Observable<any[]> {
		return this.getAction('wishlist/wishlist.php', HTTPQUERYTYPE.GET, 'getWishlist');
	}
}
