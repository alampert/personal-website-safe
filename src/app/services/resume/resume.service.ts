import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { BaseService } from '../base/base.service';
import { ToastyService } from 'ng2-toasty';
import { ServerService } from '../server/server.service';
import { HTTPQUERYTYPE, JOBTYPE } from '../../globals';
import { SkillsModel } from '../../models/skills.model';
import { JobModel } from '../../models/job.model';
import { BranchModel } from '../../models/branch.model';
import { UpperCasePipe } from '@angular/common/src/pipes';

@Injectable()
export class ResumeService extends ServerService {
	constructor(
		protected http: Http,
		protected toastyService: ToastyService
	) {
		super(http, toastyService);
	}

	getSkills(): Observable<SkillsModel[]> {
		return this.getAction('resume/resume.php', HTTPQUERYTYPE.GET, 'getSkills');
	}

	getWork(): Observable<JobModel[]> {
		return this.getAction('resume/resume.php', HTTPQUERYTYPE.GET, 'getWork');
	}

	getEducation(): Observable<JobModel[]> {
		return this.getAction('resume/resume.php', HTTPQUERYTYPE.GET, 'getEducation');
	}

	getExtra(): Observable<JobModel[]> {
		return this.getAction('resume/resume.php', HTTPQUERYTYPE.GET, 'getExtra');
	}

	getProjects(): Observable<JobModel[]> {
		return this.getAction('resume/resume.php', HTTPQUERYTYPE.GET, 'getProjects');
	}

	getAwards(): Observable<JobModel[]> {
		return this.getAction('resume/resume.php', HTTPQUERYTYPE.GET, 'getAwards');
	}

	getBranches(): Observable<BranchModel[]> {
		return this.getAction('resume/resume.php', HTTPQUERYTYPE.GET, 'getCompanyBranches');
	}

	getLanguageProgression(): Observable<SkillsModel[]> {
		return this.getAction('resume/resume.php', HTTPQUERYTYPE.GET, 'getLanguageProgression');
	}

	getPositionSkills(positionId: number): Observable<SkillsModel[]> {
		if (!positionId) {
			throw new Error('position Id was not set');
		}
		return this.getAction('resume/resume.php', HTTPQUERYTYPE.GET, `getPositionSkills&positionId=${positionId}`);
	}

	cleanJob(uncleanJob: any, branches: BranchModel[]): JobModel {
		let cleanJob: JobModel = new JobModel();

		let rawStartDates: string;
		let rawEndDates: string;
		let startDates: Date[];
		let endDates: Date[];

		cleanJob = {
			id: 0,
			name: '',
			description: '',
			type: JOBTYPE.EDUCATION,
			title: {
				full: '',
				short: ''
			},
			showInResume: false,
			skills: [],
			terms: [{
				start: new Date()
			}]
		};

		// Setting all fields to be the same
		Object.keys(cleanJob).forEach(key => {
			cleanJob[key] = uncleanJob[key];
		});

		// Setting showInResume properly
		cleanJob.showInResume = uncleanJob.showInResume == '1';

		// setting type
		let typeKey = (Object.keys(JOBTYPE).find(key => {
			return (JOBTYPE[key] == uncleanJob.positionType);
		}));

		cleanJob.type = JOBTYPE[typeKey];

		// Fixing dates

		rawStartDates = uncleanJob.start_dates;
		rawEndDates = uncleanJob.end_dates;

		if (!!rawStartDates) {
			startDates = rawStartDates.split(',').map(startDateString => {
				return new Date(startDateString);
			});
		}

		if (!!rawEndDates) {
			endDates = rawEndDates.split(',').map(endDateString => {
				return new Date(endDateString);
			});
		}

		if (!(cleanJob.terms)) {
			cleanJob.terms = [];
		}

		if (!!startDates && startDates.length > 0) {
			startDates.forEach((startDate, index) => {

				if (!!(endDates) && !!endDates[index]) {
					cleanJob.terms[index] = {
						start: startDate,
						end: endDates[index]
					};
				} else {
					cleanJob.terms[index] = {
						start: startDate
					};
				}
			});
		}

		// Adding branch to job

		cleanJob.companyBranch = branches.find(branch => {
			return branch.id == uncleanJob.branch_id;
		});

		return cleanJob;
	}

	sortJobs(unsortedJobs: JobModel[]): JobModel[] {
		unsortedJobs.sort((jobA, jobB) => {
			let jobATime = jobA && !!(jobA.terms) && jobA.terms[jobA.terms.length - 1] && !!(jobA.terms[jobA.terms.length - 1].end) ? jobA.terms[jobA.terms.length - 1].end.valueOf() : 999999999999999999999999999999;
			let jobBTime = jobB && !!(jobB.terms) && jobB.terms[jobB.terms.length - 1] && !!(jobB.terms[jobB.terms.length - 1].end) ? jobB.terms[jobB.terms.length - 1].end.valueOf() : 999999999999999999999999999999;

			if (jobATime < jobBTime) {
				return 1;
			} else if (jobATime > jobBTime) {
				return -1;
			} else {
				return 0;
			}
		});

		return unsortedJobs;
	}
}
