import { Injectable } from '@angular/core';
import { NavigationElementModel } from '../../models/navigation-element.model';
import { ToastyService } from 'ng2-toasty';
import { BaseService } from '../base/base.service';

@Injectable()
export class NavigationService extends BaseService {

	private navElements: NavigationElementModel[];

	constructor(
		protected toastyService: ToastyService
	) {
		super(toastyService);
	}

	get navigationList() {
		let list: NavigationElementModel[];

		list = [
			{
				id: 0,
				name: 'Home',
				href: '/',
				zmdiIcon: 'zmdi-home'
			},
			{
				id: 1,
				name: 'Resume',
				href: '/resume',
				zmdiIcon: 'zmdi-account-o'
			},
			{
				id: 2,
				name: 'Training',
				href: '/training',
				zmdiIcon: 'zmdi-assignment-return'
			},
			{
				id: 2,
				name: 'Wishlist',
				href: '/wishlist',
				zmdiIcon: 'zmdi-card-giftcard'
			}
		];

		return list;
	}

}
