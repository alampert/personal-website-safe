<?php

if( isset($_GET['query']) ){

	// Making sure the variables are set, and the query is recent games
	if( $_GET['query'] == 'recentgames' && isset($_GET['key']) && isset($_GET['steamid']) && isset($_GET['count']) ){

		getRecentlyPlayedGames($_GET['key'], $_GET['steamid'], $_GET['count']);

	}

}


function getRecentlyPlayedGames($key, $steamid, $count){
	$baseURL = "http://api.steampowered.com/IPlayerService/GetRecentlyPlayedGames/v0001/";

	$json = file_get_contents($baseURL . "?format=json&key=" . $key . "&steamid=" . $steamid . "&count=" . $count);

	echo($json);
}
