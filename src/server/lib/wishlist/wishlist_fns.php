<?php

include('../db_fns.php');

function getWishlistItems(){
	$query = "SELECT
		wishlist.wishlist_id,
		wishlist.title,
		wishlist.start_date,
		wishlist.end_date,
		wishlist.received,
		wishlist.where_location,
		wishlist.where_url,
		wishlist.cost,
		wishlist.priority
		FROM wishlist
		ORDER BY wishlist.priority ASC, end_date DESC, cost ASC";

	return  db_query($query);
}
