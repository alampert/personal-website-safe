<?php

include('training_fns.php');

// Get & filter GET call

$GETPARAM = $_GET;
$POSTPARAM = $_POST;

// TODO: FILTERING

$data = array("error", "data was not set");

if(isset($GETPARAM['action'])){
	if($GETPARAM['action'] == 'getCourses'){
		$data = getCourses($GETPARAM['courseId']);
	} else if ($GETPARAM['action'] == 'getCourseModules'){
		$data = getCourseModules($GETPARAM['courseId']);
	} else if ($GETPARAM['action'] == 'getCourseContent'){
		$data = getCourseContent($GETPARAM['moduleId']);
	}
} else {
	$data = array("error", "No Data");
}

echo(json_encode($data));
