<?php

include('../db_fns.php');

// FUNCTIONS

function getUsers() {
	$query = "SELECT
		27rcacs_gs.members.member_id,
		27rcacs_gs.members.rank,
		27rcacs_gs.members.first_name,
		27rcacs_gs.members.last_name
	FROM 27rcacs_gs.members";

	return  db_query($query);
}

function getUserTestAttempts($memberId) {
	$query = "SELECT
		*
	FROM 27rcacs_gs.test_attempts
	WHERE member_id = '" . $memberId . "'";

	return  db_query($query);
}

function getTests() {
	$query = "SELECT
		*
	FROM 27rcacs_gs.tests;";

	return  db_query($query);
}
