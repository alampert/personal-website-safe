<?php

include('groundschool_fns.php');

// Get & filter GET call

try {

	$GETPARAM = $_GET;
	$POSTPARAM = $_POST;

	// TODO: FILTERING

	$data = array("error", "data was not set");

	if(isset($GETPARAM['action'])){

		if($GETPARAM['action'] == 'getUsers'){

			$data = getUsers();

		} else if ($GETPARAM['action'] == 'getUserTestAttempts'){

			if(!isset($GETPARAM['member_id'])){
				throw new Exception('member_id was not set');
			}

			$data = getUserTestAttempts($GETPARAM['member_id']);

		} else if ($GETPARAM['action'] == 'getTests'){

			$data = getTests();

		}

	} else {
		throw new Exception('No Data');
	}

} catch(Exception $error) {
	$data = array("error", $error->getMessage());
}

echo(json_encode($data));
